package com.example.thinhnv.noteeveryday.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;

public class DisplayNote extends AppCompatActivity {
    Toolbar toolbar;
    NoteDBHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_note);
        toolbar=(Toolbar) findViewById(R.id.appBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Note content");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView tvDisplayTitle=(TextView) findViewById(R.id.tvDisplayTitle);
        EditText tvDisplayContent=(EditText)findViewById(R.id.tvDisplayContent);
        ImageView ivDisplayNote=(ImageView) findViewById(R.id.ivDisplayNote);
        TextView tvDisplayDate=(TextView) findViewById(R.id.tvDisplayDate);
        // process with displayNote

        dbHelper=new NoteDBHelper(this);
        Intent i=getIntent();
        long id=i.getLongExtra("MESSAGE_ID", 0);
        NoteModel model=dbHelper.getNote(id);
        tvDisplayDate.setText("Be created at:"+model.getDay()+"/"+model.getMonth()+"/2016");
        tvDisplayTitle.setText("Title: "+model.getTitle());

        if(model.getIsTextNote()){
            tvDisplayContent.setText(model.getContent());

        } else{
            tvDisplayContent.setVisibility(View.GONE);
            BitmapFactory.Options options=new BitmapFactory.Options();
            options.inSampleSize=2;
            final Bitmap bitmap=BitmapFactory.decodeFile(model.getFileImage().getPath(), options);
            ivDisplayNote.setImageBitmap(bitmap);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);

    }
}
