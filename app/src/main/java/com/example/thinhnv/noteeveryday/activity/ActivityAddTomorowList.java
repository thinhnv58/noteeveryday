package com.example.thinhnv.noteeveryday.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;

import java.util.Calendar;

public class ActivityAddTomorowList extends AppCompatActivity {

    EditText edt1,edt2,edt3,edt4,edt5;
    Button btSaveTomorowList;
    NoteDBHelper dbHelper;
    int day;
    int month;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_add_tomorow_list);
        dbHelper=new NoteDBHelper(this);

        edt1=(EditText) findViewById(R.id.edt1);
        edt2=(EditText) findViewById(R.id.edt2);
        edt3=(EditText) findViewById(R.id.edt3);
        edt4=(EditText) findViewById(R.id.edt4);
        edt5=(EditText) findViewById(R.id.edt5);
        btSaveTomorowList=(Button) findViewById(R.id.btSaveTomorowList);

        Calendar calendar=Calendar.getInstance();
        day=calendar.get(Calendar.DAY_OF_MONTH)+1;
        if(day>30){
            day=1;
        }
        month=calendar.get(Calendar.MONTH);

        //public NoteModel(String title, String content, Uri fileImage, boolean isTextNote, int day, int month)
        // create list
        btSaveTomorowList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edt1.getText().toString().equals("")){
                    dbHelper.createNote(new NoteModel(edt1.getText().toString(), "", Uri.parse(""), true, day, month));
                }if (!edt2.getText().toString().equals("")){
                    dbHelper.createNote(new NoteModel(edt2.getText().toString(), "", Uri.parse(""), true, day, month));
                }if (!edt3.getText().toString().equals("")){
                    dbHelper.createNote(new NoteModel(edt3.getText().toString(), "", Uri.parse(""), true, day, month));
                }if (!edt4.getText().toString().equals("")){
                    dbHelper.createNote(new NoteModel(edt4.getText().toString(), "", Uri.parse(""), true, day, month));
                }if (!edt5.getText().toString().equals("")){
                    dbHelper.createNote(new NoteModel(edt5.getText().toString(), "", Uri.parse(""), true, day, month));
                }

                Intent i=new Intent(ActivityAddTomorowList.this, MainActivity.class);
                startActivity(i);


            }
        });





    }
}
