package com.example.thinhnv.noteeveryday.model;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.activity.ActivityAddTomorowList;

import java.util.Calendar;

/**
 * Created by thinhnv on 29/11/2015.
 */
public class AlarmReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Calendar calendar=Calendar.getInstance();
        int hour=calendar.get(Calendar.HOUR_OF_DAY);
        int min=calendar.get(Calendar.MINUTE);
        Toast.makeText(context, hour+   ":"+min, Toast.LENGTH_SHORT).show();
        
        Intent i=new Intent(context, ActivityAddTomorowList.class);
        PendingIntent pendingIntent=PendingIntent.getActivity(context, 0, i, 0);
        NotificationCompat.Builder notiBuilder=new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_all_note)
                .setContentTitle("Not everyday")
                .setContentText("Add list note")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        NotificationManager notificationManager=(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notiBuilder.build());
    }
}
