package com.example.thinhnv.noteeveryday.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.widget.Toast;

import com.example.thinhnv.noteeveryday.model.NoteContrast.Note;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by thinhnv on 22/10/2015.
 */
public class NoteDBHelper extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="ne.db";
    // CREATE STRING SQLITE
    private static final String SQL_CREATE_DB="CREATE TABLE "+ Note.TABLE_NAME_NE+"( "+
            Note._ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Note.TITLE+" TEXT, "+
            Note.CONTENT_NE+" TEXT, "+
            Note.FILE_IMAGE_NE+" TEXT, "+
            Note.IS_TEXT_NOTE_NE+" BOOLEAN, "+
            Note.DAY_NE+" INTEGER, "+
            Note.MONTH_NE+" INTEGER )";

    private static final String SQL_DELETE_DB="DROP TABLE IF EXISTS "+Note.TABLE_NAME_NE;


    // CREATE OBJECT
    public NoteDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    public NoteDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }




    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_DB);
        onCreate(db);
    }

    // CREATE FUNCTION CONVERT
    private NoteModel populateModel(Cursor c){
        NoteModel model=new NoteModel();
        model.setId(c.getLong(c.getColumnIndex(Note._ID)));
        model.setTitle(c.getString(c.getColumnIndex(Note.TITLE)));
        model.setContent(c.getString(c.getColumnIndex(Note.CONTENT_NE)));
        model.setIsTextNote(c.getInt(c.getColumnIndex(Note.IS_TEXT_NOTE_NE)) == 0 ? false : true);
        model.setDay(c.getInt(c.getColumnIndex(Note.DAY_NE)));
        model.setMonth(c.getInt(c.getColumnIndex(Note.MONTH_NE)));
        model.setFileImage(Uri.parse(c.getString(c.getColumnIndex(Note.FILE_IMAGE_NE))));
        return model;
    }

    private ContentValues populateContent(NoteModel model){
        ContentValues values=new ContentValues();
        values.put(Note.TITLE, model.getTitle());
        values.put(Note.CONTENT_NE, model.getContent());
        values.put(Note.FILE_IMAGE_NE, model.getFileImage().toString());
        values.put(Note.IS_TEXT_NOTE_NE, model.getIsTextNote()/*model.getIsTextNote() == true ? 1 : 0*/);
        values.put(Note.DAY_NE, model.getDay());
        values.put(Note.MONTH_NE, model.getMonth());

        return values;
    }

    // SET FUNCTION CREATE ALARM, GET ALARM
    public  long createNote(NoteModel model){
        ContentValues values=populateContent(model);
        return getWritableDatabase().insert(Note.TABLE_NAME_NE, null, values);
    }

    public NoteModel getNote(long id){
        SQLiteDatabase db=this.getReadableDatabase();
        String select = "SELECT * FROM " + Note.TABLE_NAME_NE + " WHERE " + Note._ID + " = " + id;
        Cursor c=db.rawQuery(select, null);
        if (c.moveToNext()) {
            return populateModel(c);
        }
        return null;
    }

    public List<NoteModel> getNotes() {
        NoteModel modelExample;
        Calendar now=Calendar.getInstance();
        int dayNow=now.get(Calendar.DAY_OF_MONTH);
        int monthNow=now.get(Calendar.MONTH);

        SQLiteDatabase db = this.getReadableDatabase();
        String select = "SELECT * FROM " + Note.TABLE_NAME_NE;
        Cursor c = db.rawQuery(select, null);
        List<NoteModel> noteList = new ArrayList<NoteModel>();
        while (c.moveToNext()) {
            noteList.add(populateModel(c));
        }
        if (!noteList.isEmpty()) {
            return noteList;
        }
        NoteModel model=new NoteModel("Enter your note", "Note everyday",
                Uri.parse(""), true, dayNow, monthNow);
        createNote(model);
        return getNotes();
    }

    public List<NoteModel> getNotesToDay() {
        NoteModel modelExample;
        Calendar now=Calendar.getInstance();
        int dayNow=now.get(Calendar.DAY_OF_MONTH);
        int monthNow=now.get(Calendar.MONTH);


        SQLiteDatabase db = this.getReadableDatabase();
        String select = "SELECT * FROM " + Note.TABLE_NAME_NE;
        Cursor c = db.rawQuery(select, null);
        List<NoteModel> noteList = new ArrayList<NoteModel>();
        while (c.moveToNext()) {
            modelExample=populateModel(c);
            if(modelExample.getDay()==dayNow){
                noteList.add(modelExample);
            }

        }
        if (!noteList.isEmpty()) {
            return noteList;
        }
        NoteModel model=new NoteModel("Enter your note", "Note everyday",
                Uri.parse(""), true, dayNow, monthNow);
        createNote(model);
        return getNotesToDay();
    }
    public List<NoteModel> getNotesTomorow() {
        NoteModel modelExample;
        Calendar now=Calendar.getInstance();
        int dayNow=now.get(Calendar.DAY_OF_MONTH);
        int monthNow=now.get(Calendar.MONTH);


        SQLiteDatabase db = this.getReadableDatabase();
        String select = "SELECT * FROM " + Note.TABLE_NAME_NE;
        Cursor c = db.rawQuery(select, null);
        List<NoteModel> noteList = new ArrayList<NoteModel>();
        while (c.moveToNext()) {
            modelExample=populateModel(c);
            if(modelExample.getDay()==dayNow+1){
                noteList.add(modelExample);
            }

        }
        if (!noteList.isEmpty()) {
            return noteList;
        }
        NoteModel model=new NoteModel("Enter your note", "Note everyday",
                Uri.parse(""), true, dayNow+1, monthNow);
        createNote(model);
        return getNotesTomorow();
    }


    public List<NoteModel> getNotesOnDay(int dayInPut) {
        NoteModel modelExample;

        SQLiteDatabase db = this.getReadableDatabase();
        String select = "SELECT * FROM " + Note.TABLE_NAME_NE;
        Cursor c = db.rawQuery(select, null);
        List<NoteModel> noteList = new ArrayList<NoteModel>();
        while (c.moveToNext()) {
            modelExample=populateModel(c);
            if(modelExample.getDay()==dayInPut){
                noteList.add(modelExample);
            }

        }
        if (!noteList.isEmpty()) {
            return noteList;
        }
        return null;
    }

    public long updateNote(NoteModel model) {
        ContentValues values = populateContent(model);
        return getWritableDatabase().update(Note.TABLE_NAME_NE, values, Note._ID + " = ?", new String[] { String.valueOf(model.getId()) });
    }

    public int deleteNote(long id) {
        return getWritableDatabase().delete(Note.TABLE_NAME_NE, Note._ID + " = ?", new String[] { String.valueOf(id) });
    }
}
