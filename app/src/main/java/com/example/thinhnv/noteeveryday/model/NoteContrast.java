package com.example.thinhnv.noteeveryday.model;

import android.provider.BaseColumns;

/**
 * Created by thinhnv on 22/10/2015.
 */
public final class NoteContrast {
    public NoteContrast(){}

    public static abstract  class Note implements BaseColumns{
        public static final String TABLE_NAME_NE="notene";
        public static final String TITLE="titlene";
        public static final String CONTENT_NE="contentne";
        public static final String FILE_IMAGE_NE="fileimagene";
        public static final String IS_TEXT_NOTE_NE="istextnotene";
        public static final String DAY_NE="dayne";
        public static final String MONTH_NE="monthne";
    }
}
