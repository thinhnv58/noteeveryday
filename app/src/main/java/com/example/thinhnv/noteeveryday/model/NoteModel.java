package com.example.thinhnv.noteeveryday.model;

import android.net.Uri;

/**
 * Created by thinhnv on 22/10/2015.
 */
public class NoteModel {
    private long id;
    private String mTitle;
    private String mContent;
    private Uri mFileImage;
    private boolean mIsTextNote;
    private int mDay;
    private int mMonth;

    // SET CREATE
    public NoteModel(){}
    public NoteModel(String title, String content, Uri fileImage, boolean isTextNote, int day, int month){
        this.mTitle=title;
        this.mContent=content;
        this.mFileImage=fileImage;
        this.mIsTextNote=isTextNote;
        this.mDay=day;
        this.mMonth=month;
    }

    // SET VOID SET
    public void setId(long id){
        this.id=id;
    }
    public void setTitle(String title){
        this.mTitle=title;
    }
    public void setContent(String content){
        this.mContent=content;
    }
    public void setFileImage(Uri fileUri){
        mFileImage=fileUri;
    }
    public void setIsTextNote(boolean isTextNote){
        this.mIsTextNote=isTextNote;
    }
    public void setDay(int day){
        this.mDay=day;
    }
    public void setMonth(int month){
        this.mMonth=month;
    }



    // SET GET
    public long getId(){
        return this.id;
    }
    public String getTitle(){
        return mTitle;
    }
    public String getContent(){
        return this.mContent;
    }
    public Uri getFileImage(){
        return this.mFileImage;
    }
    public boolean getIsTextNote(){
        return this.mIsTextNote;
    }
    public int getDay(){
        return this.mDay;
    }
    public int getMonth(){
        return this.mMonth;
    }

}
