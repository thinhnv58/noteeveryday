package com.example.thinhnv.noteeveryday.fragment;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.model.AlarmReceiver;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNavSettime extends Fragment {
    TimePicker timePickerWakeUp;
    TimePicker timePickerSleep;
    Button btSaveTime;
    int hourWakeUp;
    int minWakeUp;
    int hourSleep;
    int minSleep;
    PendingIntent pendingIntent;
    Intent alarmIntent;
    public FragmentNavSettime() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_fragment_nav_settime, container, false);
        timePickerWakeUp=(TimePicker) view.findViewById(R.id.timePickerWakeUp);
        timePickerSleep=(TimePicker)view.findViewById(R.id.timePickerSleep);

        btSaveTime=(Button) view.findViewById(R.id.btSaveTime);

        SharedPreferences sharedPre=getActivity().getPreferences(Context.MODE_PRIVATE);
        hourWakeUp=sharedPre.getInt("HOUR_WAKE_UP", 6);
        minWakeUp=sharedPre.getInt("MIN_WAKE_UP", 0);
        hourSleep=sharedPre.getInt("HOUR_SLEEP", 23);
        minSleep=sharedPre.getInt("MIN_SLEEP", 0);
        timePickerWakeUp.setCurrentHour(hourWakeUp);
        timePickerWakeUp.setCurrentMinute(minWakeUp);
        timePickerSleep.setCurrentHour(hourSleep);
        timePickerSleep.setCurrentMinute(minSleep);


        alarmIntent=new Intent(getActivity(), AlarmReceiver.class);
        pendingIntent=PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, 0);


        btSaveTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hourWakeUp=timePickerWakeUp.getCurrentHour();
                minWakeUp=timePickerWakeUp.getCurrentMinute();
                hourSleep=timePickerSleep.getCurrentHour();
                minSleep=timePickerSleep.getCurrentMinute();

                //save to prefedent
                SharedPreferences sharedPre=getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPre.edit();
                editor.putInt("HOUR_WAKE_UP", hourWakeUp);
                editor.putInt("MIN_WAKE_UP", minWakeUp);
                editor.putInt("HOUR_SLEEP", hourSleep);
                editor.putInt("MIN_SLEEP", minSleep);
                editor.commit();


                //  set alarm service


                AlarmManager manager=(AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
                int interval=24*60*60*1000;
                manager.cancel(pendingIntent);

                Calendar calendar=Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, hourWakeUp);
                calendar.set(Calendar.MINUTE, minWakeUp);
                manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), interval, pendingIntent);




                //fragment trasition
                Toast.makeText(getActivity(), "Set time success full", Toast.LENGTH_SHORT).show();
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.replace(R.id.contentLayout, new FragmentToday());
                ft.commit();
            }
        });
        return view;
    }


}
