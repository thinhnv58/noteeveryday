package com.example.thinhnv.noteeveryday.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.activity.DisplayNote;
import com.example.thinhnv.noteeveryday.adapter.NoteListAdapter;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTomorow extends Fragment {
    private NoteDBHelper dbHelperTomorow;
    private ListView lvNoteListTomorow;
    private NoteListAdapter adapterNoteListTomorow;

    public FragmentTomorow() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view=inflater.inflate(R.layout.fragment_fragment_tomorow, container, false);
        dbHelperTomorow=new NoteDBHelper(getActivity());


        lvNoteListTomorow=(ListView) view.findViewById(R.id.lvNoteListTomorow);


        adapterNoteListTomorow=new NoteListAdapter(getActivity(), R.layout.item_note, dbHelperTomorow.getNotesTomorow());
        lvNoteListTomorow.setAdapter(adapterNoteListTomorow);
        lvNoteListTomorow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NoteModel model=dbHelperTomorow.getNote(id);

                if (model.getIsTextNote()){
                    if(model.getContent().contentEquals("")){
                        Toast.makeText(getActivity(), "Content is nothing", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                Intent i=new Intent(getActivity(), DisplayNote.class);
                i.putExtra("MESSAGE_ID", model.getId());
                startActivity(i);


            }
        });
        lvNoteListTomorow.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                dbHelperTomorow.deleteNote(id);
                adapterNoteListTomorow.notifyDataChange();
                adapterNoteListTomorow.notifyDataSetChanged();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.contentLayout, new FragmentToday());
                ft.commit();
                return true;
            }
        });
        return view;
    }

}



