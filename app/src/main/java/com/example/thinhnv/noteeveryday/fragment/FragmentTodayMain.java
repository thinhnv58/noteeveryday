package com.example.thinhnv.noteeveryday.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.activity.DisplayNote;
import com.example.thinhnv.noteeveryday.activity.MainActivity;
import com.example.thinhnv.noteeveryday.adapter.NoteListAdapter;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTodayMain extends Fragment {
    private NoteDBHelper dbHelper;
    private ListView lvNoteList;
    private NoteListAdapter adapterNoteList;
    public FragmentTodayMain() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // SET FLOAT ACTION BUTTON\
        View view=inflater.inflate(R.layout.fragment_fragment_today_main, container, false);
        dbHelper=new NoteDBHelper(getActivity());


        lvNoteList=(ListView) view.findViewById(R.id.lvNoteList);
        adapterNoteList=new NoteListAdapter(getActivity(), R.layout.item_note, dbHelper.getNotesToDay());
        lvNoteList.setAdapter(adapterNoteList);
        lvNoteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NoteModel model=dbHelper.getNote(id);

                if (model.getIsTextNote()){
                    if(model.getContent().contentEquals("")){
                        Toast.makeText(getActivity(), "Content is nothing", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                Intent i=new Intent(getActivity(), DisplayNote.class);
                i.putExtra("MESSAGE_ID", model.getId());
                startActivity(i);

            }
        });
        lvNoteList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                dbHelper.deleteNote(id);
                adapterNoteList.notifyDataChange();
                adapterNoteList.notifyDataSetChanged();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.contentLayout, new FragmentToday());
                ft.commit();
                return true;
            }
        });
        return view;
    }


}
