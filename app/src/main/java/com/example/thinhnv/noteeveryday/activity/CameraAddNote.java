package com.example.thinhnv.noteeveryday.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.adapter.NoteListAdapter;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;

import java.util.Calendar;

public class CameraAddNote extends AppCompatActivity {
    EditText editContentCamera;
    Button btSaveCameraNote;
    ImageView ivCameraNote;
    private NoteDBHelper dbHelper;

    Uri fileUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_add_note);
        Intent i=getIntent();
        String fileUriString=i.getStringExtra("ADD_CAMERA_URI");
        editContentCamera=(EditText) findViewById(R.id.editContentOfCamera);
        btSaveCameraNote=(Button) findViewById(R.id.btSaveCameraNote);
        ivCameraNote=(ImageView) findViewById(R.id.ivCameraNote);

        fileUri =Uri.parse(fileUriString);
        dbHelper=new NoteDBHelper(CameraAddNote.this);

        ivCameraNote.setImageURI( fileUri);


        btSaveCameraNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c=Calendar.getInstance();
                int day=c.get(Calendar.DAY_OF_MONTH);
                int month=c.get(Calendar.MONTH);
                String title=editContentCamera.getText().toString();
                String content="Capture image";
                NoteModel model=new NoteModel(title, content, fileUri, false, day,month );
                dbHelper.createNote(model);

                startActivity(new Intent(CameraAddNote.this, MainActivity.class));
                //finish();
            }
        });



    }
}
