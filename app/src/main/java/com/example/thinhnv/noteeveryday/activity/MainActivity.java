package com.example.thinhnv.noteeveryday.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.adapter.NoteListAdapter;
import com.example.thinhnv.noteeveryday.fragment.FragmentNavAllNote;
import com.example.thinhnv.noteeveryday.fragment.FragmentNavChangeProfile;
import com.example.thinhnv.noteeveryday.fragment.FragmentNavSettime;
import com.example.thinhnv.noteeveryday.fragment.FragmentToday;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    NoteListAdapter adapterNoteList;
    private static final String DIRECTORY_IMAGE="everyday";
    private static final int MEDIA_TYPE_IMAGE=1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE=100;
    private Uri fileUri;

    private NoteDBHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // DATABASE AND ADAPTER
        dbHelper=new NoteDBHelper(this);
        adapterNoteList=new NoteListAdapter(this, R.layout.item_note, dbHelper.getNotes());
      // SET UP TOOLBAR
        toolbar=(Toolbar) findViewById(R.id.appBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Note everyday");
        // SET DEFAULT FRAGMENT
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
        ft.add(R.id.contentLayout, new FragmentToday());
        ft.commit();


        // SET UP NAVIGATION VIEW
        navigationView=(NavigationView) findViewById(R.id.navgation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if(menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);
                drawerLayout.closeDrawers();
                FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
                Fragment fragment;
                switch (menuItem.getItemId()){
                    case R.id.nav_change_profile:
                        fragment=new FragmentNavChangeProfile();
                        break;
                    case R.id.nav_set_time:
                        fragment=new FragmentNavSettime();
                        break;
                    case R.id.nav_today:
                        fragment=new FragmentToday();
                        break;
                    case R.id.nav_all_note:
                        fragment=new FragmentNavAllNote();
                        break;
                    default:
                        Toast.makeText(MainActivity.this, "Something wrong, ", Toast.LENGTH_SHORT).show();
                        return true;
                }

                ft.replace(R.id.contentLayout, fragment);
                ft.commit();
                return true;
            }
        });
        // NAVIGATION VIEW DRAWER///

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout=(DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle
                (MainActivity.this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
                TextView tvUserName=(TextView) findViewById(R.id.username);
                TextView tvEmail=(TextView) findViewById(R.id.email);
                SharedPreferences sharedPre=getPreferences(Context.MODE_PRIVATE);
                String userName=sharedPre.getString("USER_NAME", "YourName");
                String email=sharedPre.getString("USER_EMAIL", "Email");

                String imageDecodeString=sharedPre.getString("USER_AVATAR", "");
                ImageView ivImageProfile=(ImageView) findViewById(R.id.profile_image);
                ivImageProfile.setImageBitmap(BitmapFactory.decodeFile(imageDecodeString));

                tvUserName.setText(userName);
                tvEmail.setText(email);

            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();


        ///////////////////////////////////////////////////////
        // SET FLOAT ACTION BUTTON
        ImageView ivFAB=new ImageView(this);
        ivFAB.setImageResource(R.drawable.ic_action_menu_add2);
        FloatingActionButton btFAB=new FloatingActionButton.Builder(this)
                .setContentView(ivFAB)
                .build();
        SubActionButton.Builder itemBuilder=new SubActionButton.Builder(this);
        // CREATE SUB BUTTON
        ImageView ivAddTextNote=new ImageView(this);
        ivAddTextNote.setImageResource(R.drawable.ic_action_textnote);
        SubActionButton btAddTextnote=itemBuilder.setContentView(ivAddTextNote)
                .build();


        ImageView ivAddCamera=new ImageView(this);
        ivAddCamera.setImageResource(R.drawable.ic_action_camera2);
        SubActionButton btAddCamera=itemBuilder.setContentView(ivAddCamera)
                .build();


        ImageView ivExit=new ImageView(this);
        ivExit.setImageResource(R.drawable.ic_action_exit);
        SubActionButton btExit=itemBuilder.setContentView(ivExit)
                .build();

        // CREATE RELATIVE FAB
        FloatingActionMenu btFAM=new FloatingActionMenu.Builder(this)
                .addSubActionView(btExit)
                .addSubActionView(btAddCamera)
                .addSubActionView(btAddTextnote)
                .attachTo(btFAB)
                .build();



        // SET UP LISTENER FOR BUTTONS FAB ITEM
        btAddTextnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAddNote=new Intent(MainActivity.this, ActivityAddNote.class);
                startActivityForResult(intentAddNote, 2);

            }
        });

        btAddCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri=getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        });
        btExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    // CREAE FILE URI TO STORE IMAGE
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }
    // RETURN IMAGE FILE
    private static File getOutputMediaFile(int type){
        File mediaStoreDir=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DIRECTORY_IMAGE);
        if (!mediaStoreDir.exists()) {
            if (!mediaStoreDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmSS", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type==MEDIA_TYPE_IMAGE){
            mediaFile=new File(mediaStoreDir.getPath()+File.separator+"IMG_"+timeStamp+".jpg");
        }
        else  return null;
        return mediaFile;
    }


    // SET ON ACTIVITY FOR RESULT
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Calendar now=Calendar.getInstance();
        int day=now.get(Calendar.DAY_OF_MONTH);
        int month=now.get(Calendar.MONTH);


        if (requestCode==2 && resultCode==2){
            String title=data.getStringExtra(ActivityAddNote.TITLE_ADD_NOTE);
            String content=data.getStringExtra(ActivityAddNote.CONTENT_ADD_NOTE);
            boolean isTomorow=data.getBooleanExtra(ActivityAddNote.IS_TOMOROW_NOTE, false);
            if (isTomorow) day++;

            NoteModel model=new NoteModel(title, content,
                    Uri.parse(""), true, day, month);

            dbHelper.createNote(model);
            adapterNoteList.notifyDataChange();
            adapterNoteList.notifyDataSetChanged();
            FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
            Fragment fragment=new FragmentToday();
            ft.replace(R.id.contentLayout, fragment);
            ft.commit();
        }
        if (requestCode==CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode==RESULT_OK){
            Intent intentCamera=new Intent(MainActivity.this, CameraAddNote.class);
            intentCamera.putExtra("ADD_CAMERA_URI", fileUri.toString());
            startActivity (intentCamera);




            /*String title="Camera note";
            String content="Capture image";
            NoteModel model=new NoteModel(title, content, fileUri, false, day,month );
            dbHelper.createNote(model);
            adapterNoteList.notifyDataChange();
            adapterNoteList.notifyDataSetChanged();
            FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
            Fragment fragment=new FragmentToday();
            ft.replace(R.id.contentLayout, fragment);
            ft.commit();*/
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
