package com.example.thinhnv.noteeveryday.fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.activity.DisplayEveryNote;
import com.example.thinhnv.noteeveryday.activity.MainActivity;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNavAllNote extends Fragment {
    private GridView lvAllNote;
    NoteDBHelper dbHelperAllNote;
    int firstDayOfWeek;
    public FragmentNavAllNote() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fragment_nav_all_note, container, false);

        dbHelperAllNote=new NoteDBHelper(getActivity());

        Calendar c=Calendar.getInstance();
        int month=c.get(Calendar.MONTH)+1;
        int day;

        switch (month){
            case 1:case 3:case 5:case 7:case 8:case 10:case 12:
                day=31;
                break;
            case 2:
                day=29;
                break;
            default:
                day=30;
                break;
        }

        c.set(Calendar.DAY_OF_MONTH, 1);

        firstDayOfWeek=c.get(Calendar.DAY_OF_WEEK);

        ArrayList<String> listDay=new ArrayList<String>();
        for (int i=1; i<firstDayOfWeek; i++){
            listDay.add(" ");
        }
        for (int i=0; i<day; i++){
            listDay.add(i+1+"");
        }
        ArrayAdapter<String> adapterGrid=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listDay);
        lvAllNote=(GridView) view.findViewById(R.id.lvAllNote);
        lvAllNote.setAdapter(adapterGrid);

        lvAllNote.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (dbHelperAllNote.getNotesOnDay(position + 1-(firstDayOfWeek-1)) == null) {
                    Toast.makeText(getActivity(), "No note on this day", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent i = new Intent(getActivity(), DisplayEveryNote.class);
                i.putExtra("DAY_ON_MONTH", position + 1-(firstDayOfWeek-1));
                startActivity(i);
            }
        });


        /*lvAllNote.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (dbHelperAllNote.getNotesOnDay(position + 1) == null) {
                    Toast.makeText(getActivity(), "No note on this day", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent i = new Intent(getActivity(), DisplayEveryNote.class);
                i.putExtra("DAY_ON_MONTH", position + 1);
                startActivity(i);
            }
        });
        */

        return  view;
    }
}
