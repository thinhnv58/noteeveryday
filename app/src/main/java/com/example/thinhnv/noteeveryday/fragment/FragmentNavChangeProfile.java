package com.example.thinhnv.noteeveryday.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


import com.example.thinhnv.noteeveryday.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNavChangeProfile extends Fragment {
    Button btSaveUser;
    EditText edtYourName;
    EditText edtYourEmail;
    String imgDecodableString;
    ImageButton imgbtChangeAvatar;
    String imgDecodeAvatar;

    public static int REQUEST_LOAD_IMAGE=11;
    public FragmentNavChangeProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view= inflater.inflate(R.layout.fragment_fragment_nav_change_profile, container, false);


        edtYourName=(EditText) view.findViewById(R.id.edtYourName);
        edtYourEmail=(EditText) view.findViewById(R.id.edtYourEmail);
        imgbtChangeAvatar=(ImageButton) view.findViewById(R.id.imgbtChangeAvatar);

        SharedPreferences sharedPrefe=getActivity().getPreferences(Context.MODE_PRIVATE);
        String usename=sharedPrefe.getString("USER_NAME", "Your name");
        String email=sharedPrefe.getString("USER_EMAIL", "Email");
        edtYourName.setText(usename);
        edtYourEmail.setText(email);

        SharedPreferences sharedPre=getActivity().getPreferences(Context.MODE_PRIVATE);
        imgDecodeAvatar=sharedPre.getString("USER_AVATAR", "");
        if(!imgDecodeAvatar.equals("")){
            Bitmap bitmapAvatar=BitmapFactory.decodeFile(imgDecodeAvatar);
            Bitmap bitmapAvatarScale=Bitmap.createScaledBitmap(bitmapAvatar, 500, 500, false);
            imgbtChangeAvatar.setImageBitmap(bitmapAvatarScale);
        }
        imgbtChangeAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, REQUEST_LOAD_IMAGE);
            }
        });

        btSaveUser=(Button) view.findViewById(R.id.btSaveUser);


        btSaveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharePre=getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharePre.edit();
                editor.putString("USER_NAME", edtYourName.getText().toString());
                editor.putString("USER_EMAIL", edtYourEmail.getText().toString());
                editor.commit();
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                Fragment fragment=new FragmentToday();
                ft.replace(R.id.contentLayout, fragment);
                ft.commit();
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_LOAD_IMAGE){
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            // Get the cursor
            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgDecodableString = cursor.getString(columnIndex);
            cursor.close();

            SharedPreferences sharedPrefe=getActivity().getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor=sharedPrefe.edit();
            editor.putString("USER_AVATAR", imgDecodableString);
            editor.commit();


            SharedPreferences sharedPre=getActivity().getPreferences(Context.MODE_PRIVATE);
            imgDecodeAvatar=sharedPre.getString("USER_AVATAR", "");
            Bitmap bitmapAvatar=BitmapFactory.decodeFile(imgDecodeAvatar);
            Bitmap bitmapAvatarScale=Bitmap.createScaledBitmap(bitmapAvatar, 300, 300, false);
            imgbtChangeAvatar.setImageBitmap(bitmapAvatarScale);

        }
    }
}
