package com.example.thinhnv.noteeveryday.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.thinhnv.noteeveryday.R;

public class ActivityAddNote extends AppCompatActivity {
    private EditText edtYourTitle, edtYourContentNote;
    private Button btOkAddNote;
    private CheckBox cbNoteTomorow;
    public static final String TITLE_ADD_NOTE="TITLE_ADD_NOTE";
    public static final String CONTENT_ADD_NOTE="CONTENT_ADD_NOTE";
    public static final String IS_TOMOROW_NOTE="IS_TOMOROW_NOTE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_add_note);

        edtYourContentNote=(EditText) findViewById(R.id.edtContentNote);
        edtYourTitle =(EditText) findViewById(R.id.edtEnterNote);
        btOkAddNote=(Button)findViewById(R.id.btOkAddNote);
        cbNoteTomorow=(CheckBox) findViewById(R.id.cbNoteTomorow);



        btOkAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ActivityAddNote.this, MainActivity.class);
                i.putExtra(TITLE_ADD_NOTE, edtYourTitle.getText().toString());
                i.putExtra(CONTENT_ADD_NOTE, edtYourContentNote.getText().toString());
                i.putExtra(IS_TOMOROW_NOTE, cbNoteTomorow.isChecked());
                setResult(2, i);
                finish();
            }
        });
    }
}
