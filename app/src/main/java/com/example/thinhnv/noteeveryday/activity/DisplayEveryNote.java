package com.example.thinhnv.noteeveryday.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.adapter.NoteListAdapter;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;

public class DisplayEveryNote extends AppCompatActivity {
    Toolbar toolbar;
    ListView lvEveryNote;
    NoteListAdapter adapterEveryNote;
    NoteDBHelper dbHelperEveryNote;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_every_note);
        toolbar=(Toolbar) findViewById(R.id.appBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Note");

        lvEveryNote=(ListView) findViewById(R.id.lvEveryNote);
        dbHelperEveryNote=new NoteDBHelper(this);
        Intent i=getIntent();
        int day=i.getIntExtra("DAY_ON_MONTH", 1);

        adapterEveryNote=new NoteListAdapter(this, R.layout.item_note, dbHelperEveryNote.getNotesOnDay(day));
        lvEveryNote.setAdapter(adapterEveryNote);

        lvEveryNote.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NoteModel model=dbHelperEveryNote.getNote(id);

                if (model.getIsTextNote()){
                    if(model.getContent().contentEquals("")){
                        Toast.makeText(DisplayEveryNote.this, "Content is nothing", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                Intent i=new Intent(DisplayEveryNote.this, DisplayNote.class);
                i.putExtra("MESSAGE_ID", model.getId());
                startActivity(i);
            }
        });

    }
}
