package com.example.thinhnv.noteeveryday.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;

import java.util.List;

/**
 * Created by thinhnv on 22/10/2015.
 */
public class NoteListAdapter extends BaseAdapter{
    private Context mContext;
    private List<NoteModel> listNote;
        NoteDBHelper dbHelper;

    public NoteListAdapter (Context context, int item_note, List<NoteModel> listNote){
        this.mContext=context;
        this.listNote=listNote;
        dbHelper=new NoteDBHelper(context);
    }
    @Override
    public int getCount() {
        return listNote.size();
    }

    @Override
    public Object getItem(int position) {
        return listNote.get(position);
    }
    @Override
    public long getItemId(int position) {
        return listNote.get(position).getId();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)  mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.item_note, parent, false);
        }
        NoteModel model=listNote.get(position);
        TextView tvTitleNote=(TextView) convertView.findViewById(R.id.tvTitleNote);
        tvTitleNote.setText(position+1+" "+model.getTitle());

        if(model.getIsTextNote()==false){
            ImageView ivFileImage=(ImageView) convertView.findViewById(R.id.ivFileImage);
            BitmapFactory.Options options=new BitmapFactory.Options();
            options.inSampleSize=8;
            final Bitmap bitmap=BitmapFactory.decodeFile(model.getFileImage().getPath(), options);
            ivFileImage.setImageBitmap(bitmap);
        }
        return convertView;
    }
    public void notifyDataChange(){
        listNote=dbHelper.getNotes();
    }
}
